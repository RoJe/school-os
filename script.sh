#!/bin/sh
#############################################################################
#																			#
#								Opdracht1									#
#																			#
#############################################################################
user add -m -g Robbert operator						#Maak een user aan
cd /												#Ga naar de root
cd etc/												#Ga naar de etc/
grep -n "Robbert" master.passwd >> Robbert.txt		#Vindt de regel met robbert en ouput naar Robbert.txt
vi Robbert.txt										#Open Robbert.txt
grep -n "Robbert" passwd >> Robbert2.txt			#Vindt de regel met Robbert en oupt naar Robbert
vi Robbert2.txt										# Open Robbert2.txt
#############################################################################
#																			#
#								Opdracht2									#
#																			#
#############################################################################
passwd Robbert 										# Verander wachtwoord van Robbert				
test123												# Nieuw wachtwoord van Robbert
test123												# Nieuw wachtwoord van Robbert
grep -n "Robbert" master.passwd >> Robbert-new.txt	# Vindt de regel met Robbert en ouput naar Robbert-new.txt
grep -n "Robbert" passwd >> Robbert2-new.txt		# Vindt de regel met Robbert en ouput naar Robbert2-new.txt
diff Robbert.txt Robbert-new.txt >> userdiff.txt	# Vindt de verschillen tussen 
													# Robbert.txt en Robbert-new.txt en ouput naar userdiff.txt


vi userdiff.txt										# open userdiff.txt
diff Robbert2.txt Robbert2-new.txt >> userdiff_with_password.txt 	# Vindt de verschillen tussen 
																 	# Robbert.txt en Robbert-new.txt en ouput naar userdiff.txt
vi userdiff_with_password.txt						# Open userdiff_with_password.txt
#############################################################################
#																			#
#								Opdracht3									#
#																			#
#############################################################################
find / -name stdio.h -print  >> opdracht3_1.txt			# Zoek naar het bestand stdio.h en ouput naar opdracht3_1.txt
vi opdracht3_1.txt									# Open opdracht3_1.txt
find / -name stdlib.h | xargs grep -n -r "*malloc" >> malloc_find_grep.txt	# Zoek naar stdlib.h
																					# Filter voor *malloc in bestand
																					# Output naar malloc_find_grep.txt
vi malloc_find_grep.txt																# Open malloc_find_grep.txt
#############################################################################
#																			#
#								Opdracht4									#
#																			#
#############################################################################
cd /												# Ga naar de root
mkdir -p /projects/os_opdracht1/					# Maak een nieuw reeks aan folders aan.
cd projects/os_opdracht1							# move naar de nieuw folders

echo '#include <stdio.h>' >> hello.c; 				# Echo de include stdio.h en ouput naar hello.c
echo '' >> hello.c;									# Echo een enter en ouput naar hello.c
echo 'int main(int argc, char* argv[], char* env[])' >> hello.c;	# echo de functie en ouput naar hello.c
echo '{' >> hello.c;												# echo { en ouput naar hello.c
echo 'printf("Hello world.\n");' >> hello.c; 						# echo printf en ouput naar hello.c
echo 'return 0;' >> hello.c; 										# echo return 0; en ouput naar hello.c		
echo '}' >> hello.c;												# echo einde functie en ouput naar hello.c

echo "IF NEXT FAILS: DOWNLOAD PKG clang AND binutils" 


echo "CFLAGS=-Wall -Wextra" >> MakeFile.c; 			# Echo dingen voor de MakeFile en output naar MakeFile.c
echo "hello: hello.c" >> MakeFile.c	    			# Echo het commando en bestand naam naar MakeFile.c 
make hello								   			# Gebruik het make commmando
./hello 											# Run het hallo bestand 